﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DeyiWPFClient
{
    public class PreferanceManager
    {
        //限制产生多个对象
        private static readonly PreferanceManager single = new PreferanceManager();

        private List<DeyiWPFClient.InteractModel.NewDealModel> unUploadDeals;

        private string userDefaultFile = "data/user.Default";
        private string salerIdFile = "data/saler.Default";
        private string offlineFile = "data/offline.Default";

        //E.D DEBUG
        public bool isAviable = true;
        public string salerId = "";
        public string salerName = "";

        public string password = "";
        private PreferanceManager()
        {
            if(!Directory.Exists("data"))
            {
                Directory.CreateDirectory("data");
            }
            refreshPreferance();
        }

        public static PreferanceManager getSinglton()
        {
            return single;
        }

        public void refreshPreferance()
        {
            if(File.Exists(userDefaultFile))
            {
                try
                {
                    FileStream preferanceStream = new FileStream(userDefaultFile, FileMode.Open);
                    StreamReader preferanceReader = new StreamReader(preferanceStream, Encoding.UTF8);
                    this.password = preferanceReader.ReadLine();
                    preferanceReader.Close();
                    preferanceStream.Close();
                }
                catch
                {
                    File.Delete(userDefaultFile);
                    this.password = Universal.encryptByDES("deyicard");
                }
            }
            else
            {
                this.password = Universal.encryptByDES("deyicard");
            }

            if(File.Exists(salerIdFile))
            {
                try
                {
                    FileStream preferanceStream = new FileStream(salerIdFile, FileMode.Open);
                    StreamReader preferanceReader = new StreamReader(preferanceStream, Encoding.UTF8);

                    string randomKey = preferanceReader.ReadLine();
                    string encryptedKey = preferanceReader.ReadLine();
                    string salerId = preferanceReader.ReadLine();
                    string salerName = preferanceReader.ReadLine();

                    if( Universal.encryptByDES(randomKey) == encryptedKey)
                    {
                        this.isAviable = true;
                        this.salerId = Universal.decryptByDES(salerId);
                        this.salerName = Universal.decryptByDES(salerName);
                    }

                    preferanceReader.Close();
                    preferanceStream.Close();
                }
                catch
                {
                    File.Delete(salerIdFile);
                    Universal.jumpToConsoleWithCode(Universal.salerNoVarifyArg);
                }
            }
            else
            {
                Universal.jumpToConsoleWithCode(Universal.salerNoVarifyArg);
            }
        }

        /// <summary>
        /// 更改完会自动刷新
        /// </summary>
        /// <param name="password">尚未加密的密码</param>
        public void changePassword(string password)
        {
            FileStream preferanceStream = new FileStream(userDefaultFile, FileMode.Create);
            StreamWriter preferanceWriter = new StreamWriter(preferanceStream, Encoding.UTF8);
            preferanceWriter.Write(Universal.encryptByDES(password));
            preferanceWriter.Close();
            preferanceStream.Close();
            this.refreshPreferance();
        }

        public void writeDealToOffline(DeyiWPFClient.InteractModel.NewDealModel dealToSave, string randomKey)
        {
            FileStream offlineStream;
            if(!File.Exists(this.offlineFile))
            {
                offlineStream = new FileStream(offlineFile, FileMode.Create);
            }
            else
            {
                offlineStream = new FileStream(offlineFile, FileMode.Append);
            }
            StreamWriter offlineWriter = new StreamWriter(offlineStream, Encoding.UTF8);
            string stringToWrite = randomKey + " " + Universal.encryptByDES(randomKey) + " "
                + Universal.encryptByDES(dealToSave.customerCard.ToString()) + " " + Universal.encryptByDES(dealToSave.money100.ToString());
            offlineWriter.WriteLine(stringToWrite);
            offlineWriter.Close();
            offlineStream.Close();
        }

        public List<DeyiWPFClient.InteractModel.NewDealModel> readOffLineDeals()
        {
            this.unUploadDeals = new List<InteractModel.NewDealModel>();
            if(!File.Exists(this.offlineFile))
            {
                return this.unUploadDeals;
            }
            else
            {
                FileStream offlineStream = new FileStream(offlineFile, FileMode.Open);
                StreamReader offlineReader = new StreamReader(offlineStream, Encoding.UTF8);
                string  newLine;
                while(true)
                {
                    newLine = offlineReader.ReadLine();
                    if(newLine == null || newLine ==　"")
                    {
                        break;
                    }
                    string[] newData = newLine.Split(' ');
                    string randomKey = newData[0];
                    string encrptedKey = newData[1];
                    int customerCard = int.Parse(Universal.decryptByDES(newData[2]));
                    int money100 = int.Parse(Universal.decryptByDES(newData[3]));
                    if(Universal.encryptByDES(randomKey) != encrptedKey
                        || customerCard < 0
                        || customerCard > Universal.maxCardNum
                        || money100 < 0
                        || money100 > Universal.maxMoney100)
                    {
                        offlineReader.Close();
                        offlineStream.Close();
                        File.Delete(offlineFile);
                        throw new Exception("本地数据已经损坏。部分离线数据无法上传到得益结算系统。"
                            +Universal.encryptByDES(randomKey) +" "+ encrptedKey +" " + customerCard.ToString() + " " + money100.ToString());
                    }
                    var newOfflineDeal = new DeyiWPFClient.InteractModel.NewDealModel();
                    newOfflineDeal.customerCard = customerCard;
                    newOfflineDeal.money100 = money100;
                    newOfflineDeal.salerId = int.Parse(this.salerId);
                    unUploadDeals.Add(newOfflineDeal);
                }
                offlineReader.Close();
                offlineStream.Close();
                File.Delete(offlineFile);
                return this.unUploadDeals;
            }
        }

        public void clearUploadDeals()
        {
            this.unUploadDeals = null;
        }

    }
}
