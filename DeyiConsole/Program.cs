﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using DeyiWPFClient;
using DeyiWPFClient.InteractModel;


namespace DeyiConsole
{
    class Program
    {
        static void verifySaler()
        {
            try
            {
                Console.WriteLine("检测到本台电脑尚未验证商家。按任意键进入商家认证流程。");
                Console.ReadKey();
                Console.WriteLine("输入商家ID:");
                string saler_id = Console.ReadLine();
                var salerToVarify = JsonConvert.DeserializeObject<SalerInfoModel>(BLLManager.getSingleton().querySalerInfo(saler_id, "afavadaxdxad"));
                if(salerToVarify.status == false)
                {
                    if(salerToVarify.errorMsg != null)
                    {
                        throw new Exception(salerToVarify.errorMsg);
                    }
                    throw new Exception("网络连接失败");
                }
                do{
                    Console.WriteLine("输入得益管理员给的验证码:");
                    string activate_code = Console.ReadLine();
                    if(int.Parse(activate_code) == salerToVarify.saler_activate_code)
                    {
                        FileStream preferanceStream = new FileStream("data/saler.Default", FileMode.Create);
                        StreamWriter preferanceWriter = new StreamWriter(preferanceStream, Encoding.UTF8);

                        string randomKey = "vrBCEXMsddsa";
                        preferanceWriter.WriteLine(randomKey);
                        preferanceWriter.WriteLine(Universal.encryptByDES(randomKey));
                        preferanceWriter.WriteLine(salerToVarify.saler_id);
                        preferanceWriter.WriteLine(salerToVarify.saler_name);

                        preferanceWriter.Close();
                        preferanceStream.Close();

                        Console.WriteLine("成功验证！您可以使用我们的客户端了！");
                        Console.ReadKey();
                        break;
                    }
                    else
                    {
                        Console.WriteLine("验证码错误。再试一次请输入y并回车。\n");
                    }
                } while (Console.ReadLine() == "y" || Console.ReadLine() == "Y");


                //If eq
                //Write to preferance
            }
            catch (Exception ex)
            {
                Console.WriteLine("由于发生了以下错误，未能成功认证。\n" + ex.Message);
            }
        }

        static bool downloadFile(string URL, string filename)
        {

            System.Net.HttpWebRequest Myrq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(URL);
            System.Net.HttpWebResponse myrp = (System.Net.HttpWebResponse)Myrq.GetResponse();
            System.IO.Stream st = myrp.GetResponseStream();
            System.IO.Stream so = new System.IO.FileStream(filename, System.IO.FileMode.Create);
            byte[] by = new byte[1024];
            int osize = st.Read(by, 0, (int)by.Length);
            while (osize > 0)
            {
                so.Write(by, 0, osize);
                osize = st.Read(by, 0, (int)by.Length);
            }
            so.Close();
            st.Close();
            myrp.Close();
            Myrq.Abort();
            return true;

        }

        static void update()
        {
            try
            {
                var updateModel = JsonConvert.DeserializeObject<UpdateInfoModel>(BLLManager.getSingleton().getUpdateInfo());

                if (updateModel.status == false)
                {
                    if (updateModel.errorMsg != null)
                    {
                        throw new Exception(updateModel.errorMsg);
                    }
                    throw new Exception("网络连接失败");
                }
                Console.WriteLine("正在下载升级包.....");
                if(downloadFile(updateModel.fileUrl, updateModel.fileName))
                {
                    Process.Start(updateModel.fileName,Universal.currentBigVersion.ToString());
                    Environment.Exit(0);
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("由于发生了以下错误，未能成功升级。\n" + ex.Message);
            }
        }

        static void proConsole()
        {
            Console.WriteLine("进入命令行。请输入指令。\n");
            string order;
            while((order = Console.ReadLine()) != "exit")
            {

            } 
        }

        static void Main(string[] args)
        {
            //Add New Card
            /*
            NewCardModel cardToAdd = new NewCardModel();
            cardToAdd.customerCard = 13248765;
            cardToAdd.salerId = 123;
            string result = DeyiWPFClient.BLLManager.getSingleton().addNewCard(cardToAdd,"gf9aydh3fa5");
             */
            
            //Add New Deal
            /*
            do
            {
                NewDealModel dealToAdd = new NewDealModel();
                Console.WriteLine("输入这笔交易的金额:");
                string input = Console.ReadLine();
                dealToAdd.customerCard = 12348765;
                dealToAdd.salerId = 123;
                dealToAdd.money100 = Int32.Parse(input);
                string result = DeyiWPFClient.BLLManager.getSingleton().addNewDeal(dealToAdd, "fi9rsnd19sfd8a9");
                Console.WriteLine(result);
                Console.WriteLine("\n如果要退出，输入no");
            } while (Console.ReadLine() != "no");
             * */

            //Saler records
            /*
            var query = new QueryRecordsModel();
            query.salerId = 123;
            query.beginDate = "20150101";
            query.endDate = "20151231";
            string result = DeyiWPFClient.BLLManager.getSingleton().queryInTime(query, "du0y192ehidsf");
            Console.WriteLine(result);
             * */
            
            Console.WriteLine("================得益云管理控制台================");
            Console.WriteLine("************当前得益客户端版本:" + Universal.currentBigVersion.ToString() + "." + Universal.currentSmallVersion.ToString()+"************");
            Console.WriteLine("================Code by Z.Dian================");
            if(args.Length != 0)
            {
                //自动升级处理部分
                if(args[0] == Universal.updateArg)
                {
                    update();
                }

                //商家激活处理
                else if(args[0] == Universal.salerNoVarifyArg)
                {
                    verifySaler();
                }

                //高级设置处理部分
                else if(args[0] == Universal.logoClickArg)
                {
                    proConsole();
                }

                Console.WriteLine("\n按下任意键即可退出管理控制台。");
            }
            else
            {
                
                //Process.Start("DeyiWPFClient.exe");
                //Environment.Exit(0);
                
            }
            
            
            Console.ReadKey();
        }
    }
}
