﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeyiWPFClient.InteractModel;
using Newtonsoft.Json;

namespace DeyiWPFClient
{
    public class BLLManager
    {
        private static readonly BLLManager single = new BLLManager();

        private BLLManager()
        {

        }

        public static BLLManager getSingleton()
        {
            return single;
        }

        public string addNewCard(NewCardModel newCard, string randomKey)
        {
            if(randomKey == null)
            {
                randomKey = "dnafy4qwidasfa5341dasv";
            }
            string encrptedKey = Universal.encryptByDES(randomKey);
            string result =  NetworkManager.getSinglton().sendGETRequestWithCallback(Universal.apiPath 
                + "addNewCard.aspx?rKey="+randomKey+"&eKey="+encrptedKey
                + "&data=" + JsonConvert.SerializeObject(newCard) ,5000, null);
            return result;
        }

        public string addNewDeal(NewDealModel newDeal, string randomKey)
        {
            if(randomKey == null)
            {
                randomKey = "gdiasdg2412dsar235295";
            }
            string encrptedKey = Universal.encryptByDES(randomKey);
            string result = NetworkManager.getSinglton().sendGETRequestWithCallback(Universal.apiPath
                + "addNewDeal.aspx?rKey=" + randomKey + "&eKey=" + encrptedKey
                + "&data=" + JsonConvert.SerializeObject(newDeal) , 5000, null);
            return result;
        }

        public string queryInTime(QueryRecordsModel queryData, string  randomKey)
        {
            if (randomKey == null)
            {
                randomKey = "gsalhfq75ahio1bcn83r2c";
            }
            string encrptedKey = Universal.encryptByDES(randomKey);
            string result = NetworkManager.getSinglton().sendGETRequestWithCallback(Universal.apiPath
                + "querySalerRecords.aspx?rKey=" + randomKey + "&eKey=" + encrptedKey
                + "&data=" + JsonConvert.SerializeObject(queryData), 5000, null);

            return result;
        }

        public bool uploadOffLineData(string randomKey)
        {
            var offlineDataList = PreferanceManager.getSinglton().readOffLineDeals();
            if(offlineDataList == null)
            {
                return true;
            }
            int positionFlag = 0;
            int successCount = 0;
            string errorMsgs = "";
            for(; positionFlag < offlineDataList.Count; positionFlag++)
            {
                string json = this.addNewDeal(offlineDataList[positionFlag], randomKey);
                var result = JsonConvert.DeserializeObject<RootModel>(json);
                if(result.status == false)
                {
                    //Network Off
                    if(result.errorMsg == null)
                    {
                        PreferanceManager.getSinglton().writeDealToOffline(offlineDataList[positionFlag], randomKey);
                        errorMsgs += "离线记录" + positionFlag.ToString() + ": 由于当前没有网络连接，继续保持该记录离线。\n";
                        continue;
                    }
                    else
                    {
                        errorMsgs += "离线记录" + positionFlag.ToString() + ": "+ result.errorMsg + "\n";
                        continue;
                    }
                }
                else
                {
                    successCount++;
                }
            }
            PreferanceManager.getSinglton().clearUploadDeals();
            if (successCount != offlineDataList.Count)
            {
                throw new Exception(errorMsgs);
            }
            else
                return true;
        }

        public string getUpdateInfo()
        {
            return NetworkManager.getSinglton().sendGETRequestWithCallback(Universal.apiPath
                + "checkUpdate.aspx", 5000, null);
        }

        public string querySalerInfo(string saler_id,string randomKey)
        {
            if (randomKey == null)
            {
                randomKey = "asdav23cdf235c32dx";
            }
            string encrptedKey = Universal.encryptByDES(randomKey);
            string result = NetworkManager.getSinglton().sendGETRequestWithCallback(Universal.apiPath
                + "getSalerInfoById.aspx?rKey=" + randomKey + "&eKey=" + encrptedKey
                + "&saler_id=" + saler_id, 5000, null);
            return result;
        }


    }
}
