﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace DeyiWPFClient
{
    public static class Universal
    {
        //Version Info. Add 1 to bigVersion means the old is no longer supported
        public static int currentBigVersion = 1;
        public static int currentSmallVersion = 0;

        //Server Settings
        public static string serverPath = "http://123.56.144.82/";
        public static string apiPath = serverPath + "WinApi/";
        //public static string apiPath = "http://localhost:24614/WinApi/";//Debug only
        public const int maxCardNum = 99999999;
        public const int maxMoney100 = 500000;

        //Special Args for Console
        public static string logoClickArg = "RIGHT_CLICK_LOGO";
        public static string updateArg = "UPDATE_AVALIABLE";
        public static string salerNoVarifyArg = "SALER_NOT_VARIFY";

        //E.D M.S 这里的key用于本地配置的加密和网络传输的加密，小心！
        private static string initKey = "justASimpleKey";
        private static byte[] DESKey = Encoding.UTF8.GetBytes(initKey.Substring(0,8));
        private static byte[] DESIV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
        public static string encryptByMD5(string str)
        {
            MD5 md5 = MD5.Create();
            byte[] encodedBytes = md5.ComputeHash(Encoding.UTF8.GetBytes(str));
            return Encoding.UTF8.GetString(encodedBytes);
        }

        public static string encryptByDES(string encryptString)
        {
            try
            {
                byte[] rgbKey = DESKey;
                byte[] rgbIV = DESIV;
                byte[] inputByteArray = Encoding.UTF8.GetBytes(encryptString);
                DESCryptoServiceProvider dCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, dCSP.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return Convert.ToBase64String(mStream.ToArray());
            }
            catch
            {
                return encryptString;
            }
        }

        public static string decryptByDES(string decryptString)
        {
            try
            {
                byte[] rgbKey =  DESKey;
                byte[] rgbIV = DESIV;
                byte[] inputByteArray = Convert.FromBase64String(decryptString);
                DESCryptoServiceProvider DCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, DCSP.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return Encoding.UTF8.GetString(mStream.ToArray());
            }
            catch
            {
                return decryptString;
            }
        }

        public static int? toMoney100(string moneyWithDot)
        {
            try
            {
                if (moneyWithDot.IndexOf('.') > -1
                    || moneyWithDot.IndexOf('。') > -1)
                {
                    string[] twoparts = moneyWithDot.Split('.', '。');
                    if(twoparts[1].Length == 1)
                    {
                        return Int32.Parse(twoparts[0] + twoparts[1] + "0");
                    }
                    return Int32.Parse(twoparts[0] + twoparts[1].Substring(0,2));
                }
                else
                    return Int32.Parse(moneyWithDot + "00");
            }
            catch
            {
                return null;
            }

        }

        public static string toDisplayMoney(long money100)
        {
            return (money100 / 100).ToString() + "." + (money100 % 100).ToString();
        }

        public static string toNaturalTime(string time)
        {
            return time.Substring(0,2) + ":" +time.Substring(2,2) +":" +time.Substring(4,2);
        }

        public static void jumpToConsoleWithCode(string code)
        {
            
            System.Diagnostics.Process.Start("DeyiConsole.exe",code);
            Environment.Exit(0);
        }
    }
}
