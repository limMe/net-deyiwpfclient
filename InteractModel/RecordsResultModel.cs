﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeyiWPFClient.InteractModel
{
    public class RecordsResultModel : RootModel
    {
        public List<RecordItem> recordsList { get; set; }
    }

    public class RecordItem
    {
        public long salerId { get; set; }

        public long dealId { get; set; }

        public long userCard { get; set; }

        public int amount { get; set; }

        public string date { get; set; }

        public string time { get; set; }
    }
}
