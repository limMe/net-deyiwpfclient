﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeyiWPFClient.InteractModel
{
    public class QueryRecordsModel : RootModel
    {
        public int salerId { get; set; }
        public string beginDate { get; set; }
        public string endDate { get; set; }
    }
}
