﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DeyiWPFClient.InteractModel;
using Newtonsoft.Json;

namespace DeyiWPFClient
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {

        DispatcherTimer loginAnimateTimer = new DispatcherTimer();
        DispatcherTimer logoffAnimateTimer = new DispatcherTimer();
        private appState runningState = appState.notLogin;

        public MainWindow()
        {
            InitializeComponent();
        }

        public void changeState(appState fromState, appState toState)
        {
            runningState = toState;
            if(fromState == appState.notLogin && toState == appState.logined)
            {
                loginAnimateTimer.Start();
                BluredBg.Visibility = System.Windows.Visibility.Visible;
                try
                {
                    BLLManager.getSingleton().uploadOffLineData("asbfqn49qoc7n391d");
                }
                catch(Exception ex)
                {
                    MessageBox.Show("尝试上传上次网络故障时的数据出现了以下问题，部分订单未能成功上传:\n" + ex.Message);
                }
                //leftMenu.Visibility = System.Windows.Visibility.Visible; was put in the end of animation
            }
            else if(fromState == appState.logined && toState == appState.notLogin)
            {
                clearBg.Visibility = System.Windows.Visibility.Visible;
                leftMenu.Visibility = System.Windows.Visibility.Hidden;
               
                logoffAnimateTimer.Start();
            }
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PreferanceManager.getSinglton();
            var updateModel = JsonConvert.DeserializeObject<UpdateInfoModel>(BLLManager.getSingleton().getUpdateInfo());
            if(updateModel.status == true)
            {
                if(updateModel.currentBigVersion > Universal.currentBigVersion)
                {
                    Universal.jumpToConsoleWithCode(Universal.updateArg);
                }
            }

            loginAnimateTimer.Tick += new EventHandler(loginAnimateTimer_Tick);
            loginAnimateTimer.Interval = TimeSpan.FromSeconds(0.02);
            logoffAnimateTimer.Tick += new EventHandler(logoffAnimateTimer_Tick);
            logoffAnimateTimer.Interval = TimeSpan.FromSeconds(0.02);
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            
        }

        private void loginAnimateTimer_Tick(object sender, EventArgs e)
        {
            if (BluredBg.Opacity > 0.9)
            {
                loginAnimateTimer.Stop();
                clearBg.Visibility = System.Windows.Visibility.Hidden;
                loginPanel.Visibility = System.Windows.Visibility.Hidden;
                leftMenu.Visibility = System.Windows.Visibility.Visible;
            }
            BluredBg.Opacity += 0.05;
        }

        private void logoffAnimateTimer_Tick(object sender, EventArgs e)
        {
            if (BluredBg.Opacity < 0.1)
            {
                logoffAnimateTimer.Stop();
                BluredBg.Visibility = System.Windows.Visibility.Hidden;
                loginPanel.Visibility = System.Windows.Visibility.Visible;
            }
            BluredBg.Opacity -= 0.05;
        }

        private void LogBtn_Click(object sender, RoutedEventArgs e)
        {
            if(Universal.encryptByDES(pwdBox.Password)==PreferanceManager.getSinglton().password)
            {
                changeState(appState.notLogin,appState.logined);
            }
            else
            {
                MessageBox.Show("密码错误，请重试。");
            }
            pwdBox.Password = "";
        }

        private void logOffBtn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            hideAllFuncPanel();
            changeState(appState.logined, appState.notLogin);
        }

        private void pwdBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.LogBtn_Click(pwdBox, null);
            }
        }

        private void cardNumBox_later_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyStates == Keyboard.GetKeyStates(Key.Q) && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (cardNumBox_former.IsEnabled == true)
                {
                    cardNumBox_former.IsEnabled = false;
                }
                else
                {
                    cardNumBox_former.IsEnabled = true;
                    cardNumBox_former.Text = "";
                    cardNumBox_former.Focus();
                }
            }

        }

        private void cardNumBox_later_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(cardNumBox_later.Text.Length == cardNumBox_later.MaxLength)
            {
                moneyBox.Focus();
            }
        }

        private void cardNumBox_former_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(cardNumBox_former.Text.Length == cardNumBox_former.MaxLength)
            {
                cardNumBox_later.Focus();
            }
        }

        private void moneyBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.submitNewDeal_MouseUp(moneyBox, null);
            }
        }

       
        private void submitNewDeal_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string cardStr = this.cardNumBox_former.Text + this.cardNumBox_later.Text;
                if (this.cardNumBox_former.Text.Length != this.cardNumBox_former.MaxLength
                    || this.cardNumBox_later.Text.Length != this.cardNumBox_later.MaxLength)
                {
                    throw new Exception("卡号格式不对。得益卡都是8位整数。");
                }
                int cardInt = Int32.Parse(cardStr);
                int? money100 = Universal.toMoney100(this.moneyBox.Text);
                if (money100 != null)
                {
                    if(money100 > Universal.maxMoney100)
                    {
                        throw new Exception("超过了当前设置的最大单笔金额：" + (Universal.maxMoney100 / 100).ToString() + "元人民币");
                    }
                    var dealToSend = new NewDealModel();
                    dealToSend.customerCard = cardInt;
                    dealToSend.money100 = (int)money100;
                    dealToSend.salerId = Int32.Parse(PreferanceManager.getSinglton().salerId);
                    string json = BLLManager.getSingleton().addNewDeal(dealToSend, "baiuyd2ensd6a96");
                    var result = JsonConvert.DeserializeObject<RootModel>(json);
                    if (result.status == true)
                    {
                        MessageBox.Show("交易已经成功写入数据库");
                        this.cardNumBox_later.Text = "";
                        this.moneyBox.Text = "";
                    }
                    else if(result.errorMsg != null)
                    {
                        throw new Exception(result.errorMsg);
                    }
                    else
                    {
                        PreferanceManager.getSinglton().writeDealToOffline(dealToSend, "sagdhopqf123");
                        this.cardNumBox_later.Text = "";
                        this.moneyBox.Text = "";
                        throw new Exception("网络连接错误，请检查你的网络设置。已经将这笔交易写入本地缓存，将在下次启动本应用时自动上传，请勿重复提交。");
                    }
                }
                else
                    throw new Exception("卡号或者金额输入错误!");
            }
            catch(Exception ex)
            {
                MessageBox.Show("由于发生了以下错误，交易未能成功提交\n\n" + ex.Message);
            }
        }

        private void submitNewCard_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                int card_num = int.Parse(this.newCardBox.Text);
                if(this.newCardBox.Text.Length !=8)
                {
                    throw new Exception("卡号位数不对。所有得益卡都应该是8位。");
                }
                if(card_num>99999999 || card_num < 0)
                {
                    throw new Exception("卡号输入超过了限制");
                }
                var cardModelToSend = new NewCardModel();
                cardModelToSend.customerCard = card_num;
                cardModelToSend.salerId = int.Parse(PreferanceManager.getSinglton().salerId);
                string json = BLLManager.getSingleton().addNewCard(cardModelToSend, "sahdy212n938e1");
                var result = JsonConvert.DeserializeObject<AddCardResultModel>(json);
                if(result.status == true)
                {
                    MessageBox.Show("发卡成功！请以下信息告诉用户，以便于在微信上绑定并使用我们的服务。\n\n卡号："
                        +cardModelToSend.customerCard.ToString() + "\n验证码："+result.verifyNum.ToString());
                }
                else
                {
                    throw new Exception(result.errorMsg);
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show("由于以下错误，未能成功新发卡片:\n\n" + ex.Message);
            }
        }

        private void newCardBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.submitNewCard_MouseUp(newCardBox, null);
            }
        }

        private void submitQuery_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var queryToSend = new QueryRecordsModel();
                int beginDate = int.Parse(this.beginDateBox.Text);
                int endDate = int.Parse(this.endDateBox.Text);
                if(beginDate < 20100000 || beginDate > 20501231 || endDate < 20100000 || endDate > 20501231)
                {
                    throw new Exception("日期输入错误。格式应如同20150229");
                }
                queryToSend.beginDate = this.beginDateBox.Text;
                queryToSend.endDate = this.endDateBox.Text;
                queryToSend.salerId = Int32.Parse(PreferanceManager.getSinglton().salerId);
                string result = BLLManager.getSingleton().queryInTime(queryToSend, "bwdoasm872c4");
                //MessageBox.Show(result);
                var resultObj = JsonConvert.DeserializeObject<RecordsResultModel>(result);
                if(resultObj.status == false)
                {
                    if(resultObj.errorMsg == null)
                    {
                        throw new Exception("网络连接失败");
                    }
                    throw new Exception(resultObj.errorMsg);
                }

                string fileName = beginDate.ToString() + "至" + endDate.ToString()+"的账单记录.txt";
                System.IO.FileStream resultFileStream = new System.IO.FileStream("data/" + fileName, System.IO.FileMode.Create);
                System.IO.StreamWriter resultFileWriter = new System.IO.StreamWriter(resultFileStream, Encoding.UTF8);
                resultFileWriter.WriteLine("订单号|\t------时间------|顾客卡号|\t金额");

                long totalMoney = 0;
                for(int i=0; i<resultObj.recordsList.Count; i++)
                {
                    resultFileWriter.WriteLine(resultObj.recordsList[i].dealId.ToString()
                        + "\t" + resultObj.recordsList[i].date + " "
                        + Universal.toNaturalTime(resultObj.recordsList[i].time)
                        + "\t" +resultObj.recordsList[i].userCard.ToString()
                        + "\t" +Universal.toDisplayMoney(resultObj.recordsList[i].amount));
                    totalMoney = totalMoney + resultObj.recordsList[i].amount;
                }
                resultFileWriter.WriteLine("\n=========本期总计计分金额" + Universal.toDisplayMoney(totalMoney) + "元=============");
                resultFileWriter.Close();
                resultFileStream.Close();
                System.Diagnostics.Process.Start("notepad.exe","data/"+ fileName);
            }
            catch(Exception ex)
            {
                MessageBox.Show("由于发生了以下错误，未能获得该查询:\n\n" + ex.Message);
            }
        }

        private void endDateBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.submitQuery_MouseUp(endDateBox, null);
            }
        }

        private void beginDateBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.submitQuery_MouseUp(beginDateBox, null);
            }
        }

        private void submitSetting_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (Universal.encryptByDES(this.OldPwdBox.Password) == PreferanceManager.getSinglton().password)
            {
                if (this.NewPwdBox.Password == this.RepeatNewPwdBox.Password)
                {
                    if(this.NewPwdBox.Password == "")
                    {
                        MessageBox.Show("密码不能为空");
                        return;
                    }

                    PreferanceManager.getSinglton().changePassword(this.NewPwdBox.Password);
                    this.OldPwdBox.Password = "";
                    this.NewPwdBox.Password = "";
                    this.RepeatNewPwdBox.Password = "";
                    MessageBox.Show("修改密码成功！");
                }
                else
                {
                    MessageBox.Show("两次输入的密码不一样。未能成功更改密码");
                }
            }
            else
                MessageBox.Show("当前密码校验错误。未能成功更改密码");
        }

        private void RepeatNewPwdBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.submitSetting_MouseUp(RepeatNewPwdBox, null);
            }
        }

        private void hideAllFuncPanel()
        {
            this.NewCardPanel.Visibility = System.Windows.Visibility.Hidden;
            this.NewDealPanel.Visibility = System.Windows.Visibility.Hidden;
            this.QueryPanel.Visibility = System.Windows.Visibility.Hidden;
            this.SettingPanel.Visibility = System.Windows.Visibility.Hidden;
        }

        private void AddNewDealLeft_MouseUp(object sender, MouseButtonEventArgs e)
        {
            hideAllFuncPanel();
            this.NewDealPanel.Visibility = System.Windows.Visibility.Visible;
        }

        private void AddNewCardLeft_MouseUp(object sender, MouseButtonEventArgs e)
        {
            hideAllFuncPanel();
            this.NewCardPanel.Visibility = System.Windows.Visibility.Visible;
        }

        private void QueryLeft_MouseUp(object sender, MouseButtonEventArgs e)
        {
            hideAllFuncPanel();
            this.QueryPanel.Visibility = System.Windows.Visibility.Visible;
        }

        private void SettingLeft_MouseUp(object sender, MouseButtonEventArgs e)
        {
            hideAllFuncPanel();
            this.SettingPanel.Visibility = System.Windows.Visibility.Visible;
        }

        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void LogoBtn_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Universal.jumpToConsoleWithCode(Universal.logoClickArg);
        }

    }

    public enum appState
    { 
        notLogin = 1,
        logined = 2,
    }
}
